import TodoItem from './TodoItem/TodoItem'
import todosData from './TodoItem/todosData'

const TodoList = () => {
	return (
    <div>
      {todosData.map((item) => {
        return <TodoItem description={item.text} key={item.id} status={item.status} />
      })}
    </div>
  )
}

export default TodoList