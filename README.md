# Learn homeWork 5.3 by Ildar Amirov

See here - https://
тут можно оставить ссылку на уже выложенный рабочий код при неоходимости

## Available Scripts

React 18, sass......
тут наши технологии можно описать. но сейчас кроме реакта ничего нет

### Getting Started

Clone this project and install dependencies:

```bash
npm i
# or
yarn install
```
Add your environment variables (nextAuth\NotionApi) .....когда будут, то будем добавлять

Runs the app in the development mode:

```bash
npm run start
# or
yarn start
```

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### Folder Structure
 
| File/Folder  	   									| Primary use    																								|
| :-------------------------------- | :------------------------------------------------------------ |
| `/src`				          					| Main source folder client components  			  					  		|
| `/public`          			 					| All of our static files																				|
| `/src/components`       					| React components    																					|
| `/src/ui`       									| UI/UX components    																					|
| `/src/hooks`       								| Custom hooks		    																					|
| `/src/utils`			           			| Config file for SEO MetaData 																  |
| `/src/styles`			         				| Css global styling 																						|


![Screenshot 1](/public/shots/shot1.jpg)

### Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
To learn React, check out the [React documentation](https://reactjs.org/).

### Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.
